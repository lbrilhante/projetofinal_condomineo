/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controler;
import Models.Sugestoes;
import java.util.ArrayList;
/**
 *
 * @author Lucas
 */
public class ControleSugestoes {
    private ArrayList<Sugestoes> Blog;

    public ControleSugestoes() {
        Blog = new ArrayList<Sugestoes>();
    }
    
    public void adicionar(Sugestoes umaSugestao) {
 
        Blog.add(umaSugestao);
	
    }
    public void remover(Sugestoes umaSugestao) {

        Blog.remove(umaSugestao);
        
    }
    public Sugestoes pesquisarSugestao(String umComentario) {

        for (Sugestoes umaSugestao: Blog) {
            
           if (umaSugestao.getComentario().equalsIgnoreCase(umComentario)) 
               return umaSugestao;
           
        }
        return null;
    }

    public ArrayList<Sugestoes> getBlog() {
        return Blog;
    }

    public void setBlog(ArrayList<Sugestoes> Blog) {
        this.Blog = Blog;
    }
    
}
