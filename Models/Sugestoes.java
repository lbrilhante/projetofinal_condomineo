/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Lucas
 */
public class Sugestoes {
    //atributos
    private Morador umMorador;
    private String Comentario;
    private String data;
    
    public Sugestoes() { 
    }
    
    
    //metodos
    public Morador getUmMorador() {
        return umMorador;
    }

    public void setUmMorador(Morador umMorador) {
        this.umMorador = umMorador;
    }

    public String getComentario() {
        return Comentario;
    }

    public void setComentario(String Comentario) {
        this.Comentario = Comentario;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
    
    
}
