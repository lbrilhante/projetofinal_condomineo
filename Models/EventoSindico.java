/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Lucas
 */
public class EventoSindico extends Evento{
    private String Horario;
    public EventoSindico(String Tipo, String Complemento, String data, String umMorador, String Horario) {
        super(Tipo, Complemento, data, umMorador);
        this.Horario = Horario;
        Tipo = "Reuniao";
    }

    public String getHorario() {
        return Horario;
    }

    public void setHorario(String Horario) {
        this.Horario = Horario;
    }
    
}
