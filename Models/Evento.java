/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * @author Lucas
 */
public abstract class Evento {
    //atributos
    private String Tipo;
    private String Complemento;
    private String data;
    private String umMorador;
    //contrutores
    public Evento() { 
    }
    public Evento(String Tipo, String Complemento, String data, String umMorador) {
        this.Tipo = Tipo;
        this.Complemento = Complemento;
        this.data = data;
        this.umMorador = umMorador;
    }
    //metodos
    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public String getComplemento() {
        return Complemento;
    }

    public void setComplemento(String Complemento) {
        this.Complemento = Complemento;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getUmMorador() {
        return umMorador;
    }

    public void setUmMorador(String umMorador) {
        this.umMorador = umMorador;
    }
    
    
}
