/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controler;
import Models.Morador;
import java.util.ArrayList;

/**
 *
 * @author Lucas
 */
public class ControleMorador {
   
    //atributos
    protected ArrayList<Morador> Moradores;

    //contrutor
    public ControleMorador() {
        this.Moradores = new ArrayList<Morador>();
    }

    //metodos
    public ArrayList<Morador> getMoradores() {
        return Moradores;
    }

    public void setMoradores(ArrayList<Morador> Moradores) {
        this.Moradores = Moradores;
    }
    
    
    public void adicionar(Morador umMorador) {
        Moradores.add(umMorador);
	
    }
    public void remover(Morador umMorador) {
        Moradores.remove(umMorador);

    }
    public Morador pesquisarCpf(String umCpf) {

        for (Morador umMorador: Moradores) {
            
           if (umMorador.getCpf().equalsIgnoreCase(umCpf)) 
               return umMorador;
           
        }
        return null;
    }
    public Morador pesquisarLogin(String umLogin) {

        for (Morador umMorador: Moradores) {
            
           if (umMorador.getLogin().equalsIgnoreCase(umLogin)) {
               return umMorador;
           }
           
        }
    return null;    
    }
    
}
