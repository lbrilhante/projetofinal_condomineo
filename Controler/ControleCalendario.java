/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controler;
import Models.*;
import java.util.ArrayList;
/**
 *
 * @author Lucas
 */
public class ControleCalendario {
    //atrubutos
    protected ArrayList<EventoChurrasqueira> Churrasqueira;
    protected ArrayList<EventoSalaoDeFesta> SalaoDeFesta;
    protected ArrayList<EventoSalaDeCinema> SalaDeCinema;
    protected ArrayList<EventoSindico> Reuniao;
//contrutor
    public ControleCalendario() {
         this.Churrasqueira = new ArrayList<EventoChurrasqueira>();
         this.SalaoDeFesta = new ArrayList<EventoSalaoDeFesta>();
         this.SalaDeCinema = new ArrayList<EventoSalaDeCinema>();
         this.Reuniao = new ArrayList<EventoSindico>();
    }
    //Metodos
    public void adicionarEvento(EventoChurrasqueira umEvento) {
         Churrasqueira.add(umEvento);
    }
    public void adicionarEvento(EventoSalaoDeFesta umEvento) {
        SalaoDeFesta.add(umEvento);
    }
    public void adicionarEvento(EventoSalaDeCinema umEvento) {
        SalaDeCinema.add(umEvento);
    }
    public void adicionarEvento(EventoSindico umEvento) {
        Reuniao.add(umEvento);
    }
    
    public void removerEvento(EventoChurrasqueira umEvento) {
        Churrasqueira.remove(umEvento);
    }
    public void removerEvento(EventoSalaoDeFesta umEvento) {
        SalaoDeFesta.remove(umEvento);
    }
    public void removerEvento(EventoSalaDeCinema umEvento) {
        SalaDeCinema.remove(umEvento);
    }
    public void removerEvento(EventoSindico umEvento) {
        Reuniao.remove(umEvento);
    }
    
    
    public EventoChurrasqueira pesquisarchurrasqueira(String umaData){
   
            for (EventoChurrasqueira umEvento: Churrasqueira) {

               if (umEvento.getData().equalsIgnoreCase(umaData)) 
                   return umEvento;

                }
            
            return null;
    }
    public EventoSalaoDeFesta pesquisarSalaoDeFesta(String umaData){
   
            for (EventoSalaoDeFesta umEvento: SalaoDeFesta) {

               if (umEvento.getData().equalsIgnoreCase(umaData)) 
                   return umEvento;

                }
            
            return null;
    }
    public EventoSalaDeCinema pesquisarSalaDeCinema(String umaData) {

       
            for (EventoSalaDeCinema umEvento: SalaDeCinema) {

               if (umEvento.getData().equalsIgnoreCase(umaData)) 
                   return umEvento;

                }
            
            
        return null;
        }
    public EventoSindico pesquisarReuniao(String umaData) {

       
            for (EventoSindico umEvento: Reuniao) {

               if (umEvento.getData().equalsIgnoreCase(umaData)) 
                   return umEvento;

                }
            
            
        return null;
        }
    // pesquisa por nome
    public EventoChurrasqueira pesquisarchurrasqueiraNome(String umNome){
            
            for (EventoChurrasqueira umEvento: Churrasqueira) {

               if (umEvento.getUmMorador().equalsIgnoreCase(umNome)) 
                   return umEvento;

                }
            
            return null;
    }
    public EventoSalaoDeFesta pesquisarSalaoDeFestaNome(String umNome){
   
            for (EventoSalaoDeFesta umEvento: SalaoDeFesta) {

               if (umEvento.getUmMorador().equalsIgnoreCase(umNome)) 
                   return umEvento;

                }
            
            return null;
    }
    public EventoSalaDeCinema pesquisarSalaDeCinemaNome(String umNome) {

       
            for (EventoSalaDeCinema umEvento: SalaDeCinema) {

               if (umEvento.getUmMorador().equalsIgnoreCase(umNome)) 
                   return umEvento;

                }
            
            
        return null;
        }
    public EventoSindico pesquisarReuniaoNome(String umNome) {

       
            for (EventoSindico umEvento: Reuniao) {

               if (umEvento.getUmMorador().equalsIgnoreCase(umNome)) 
                   return umEvento;

                }
            
            
        return null;
        }

    public ArrayList<EventoChurrasqueira> getChurrasqueira() {
        return Churrasqueira;
    }

    public void setChurrasqueira(ArrayList<EventoChurrasqueira> Churrasqueira) {
        this.Churrasqueira = Churrasqueira;
    }

    public ArrayList<EventoSalaoDeFesta> getSalaoDeFesta() {
        return SalaoDeFesta;
    }

    public void setSalaoDeFesta(ArrayList<EventoSalaoDeFesta> SalaoDeFesta) {
        this.SalaoDeFesta = SalaoDeFesta;
    }

    public ArrayList<EventoSalaDeCinema> getSalaDeCinema() {
        return SalaDeCinema;
    }

    public void setSalaDeCinema(ArrayList<EventoSalaDeCinema> SalaDeCinema) {
        this.SalaDeCinema = SalaDeCinema;
    }

    public ArrayList<EventoSindico> getReuniao() {
        return Reuniao;
    }

    public void setReuniao(ArrayList<EventoSindico> Reuniao) {
        this.Reuniao = Reuniao;
    }
    
}
