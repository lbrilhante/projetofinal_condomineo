/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;


/**
 *
 * @author Lucas
 */
public class Morador {
    //atributos
    private String Nome;
    private String Apartamento;
    private String Login;
    private String Senha;
    String Credencial;
    private String Cpf;
    //contrutor
    public Morador(){
    }
    
    public Morador(String Nome, String Apartamento, String Login, String Senha,String Credencial,String Cpf) {
        this.Nome = Nome;
        this.Apartamento = Apartamento;
        this.Login = Login;
        this.Senha = Senha;
        this.Credencial = Credencial;
        this.Cpf = Cpf;
    }
    //metodos
    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getApartamento() {
        return Apartamento;
    }

    public void setApartamento(String Apartamento) {
        this.Apartamento = Apartamento;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getSenha() {
        return Senha;
    }

    public void setSenha(String Senha) {
        this.Senha = Senha;
    }

    public String getCredencial() {
        return Credencial;
    }

    public void setCredencial(String Credencial) {
        this.Credencial = Credencial;
    }

    public String getCpf() {
        return Cpf;
    }

    public void setCpf(String Cpf) {
        this.Cpf = Cpf;
    }
    
    
}
